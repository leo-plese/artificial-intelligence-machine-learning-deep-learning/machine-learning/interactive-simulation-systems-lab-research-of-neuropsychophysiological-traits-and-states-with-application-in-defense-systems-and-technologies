# Interactive Simulation Systems Lab Research of Neuropsychophysiological Traits and States with Application in Defense Systems and Technologies

Research based on experience gained from active participation in Interactive Simulation Systems Lab at FER faculty, Zagreb in subject Defense Systems and Technologies.

It consists of two main series of experiments measuring neuropsychophysiological variables of examinee during the lab signal measurements, in particular it is measuring I) resilience and II) cognitive load.

The research is an interdisciplinary one since it focuses on machine learning techniques and tools with objective of acquiring valuable insights into human examinees' neuropyschophysiology. These insights contribute to many real-world applications such as those in Defense Systems but also in other areas such as selection of the best candidates for highly-stressful professions as for instance special forces, (military) pilot, air traffic controller.

Task description in "TaskSpecification.pdf".

Project duration: Nov 2021 - Jan 2022.
